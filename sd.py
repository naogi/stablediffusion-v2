import os
import torch
import numpy as np
from omegaconf import OmegaConf
from PIL import Image
from tqdm import tqdm, trange
from itertools import islice
from einops import rearrange
from torchvision.utils import make_grid
from pytorch_lightning import seed_everything
from torch import autocast
from contextlib import nullcontext

from ldm.util import instantiate_from_config
from ldm.models.diffusion.ddim import DDIMSampler
from ldm.models.diffusion.plms import PLMSSampler
from ldm.models.diffusion.dpm_solver import DPMSolverSampler

from config import Config

class Stablediffusion():
    def chunk(self, it, size):
        it = iter(it)
        return iter(lambda: tuple(islice(it, size)), ())


    def load_model_from_config(self, config, ckpt, verbose=False):
        print(f"Loading model from {ckpt}")
        pl_sd = torch.load(ckpt, map_location="cpu")

        if "global_step" in pl_sd:
            print(f"Global Step: {pl_sd['global_step']}")

        sd = pl_sd["state_dict"]
        model = instantiate_from_config(config.model)
        m, u = model.load_state_dict(sd, strict=False)

        if len(m) > 0 and verbose:
            print("missing keys:")
            print(m)

        if len(u) > 0 and verbose:
            print("unexpected keys:")
            print(u)

        model.cuda()
        model.eval()

        return model

    def save_sample(self, path, base_count, sample):
      sample = 255. * rearrange(sample.cpu().numpy(), 'c h w -> h w c')
      img = Image.fromarray(sample.astype(np.uint8))
      sample_path = os.path.join(path, f"{base_count:05}.png")

      img.save(sample_path)

      return sample_path

    def __init__(self, ckpt:str):
        self.opt = Config(ckpt=ckpt).call()

        config = OmegaConf.load(f"{self.opt.config}")
        model = self.load_model_from_config(config, f"{self.opt.ckpt}")

        self.device = torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")
        self.model = model.to(self.device)

    def call(self, outdir:str, seed:int, prompt:str) -> None:
      seed_everything(self.opt.seed)

      self.opt.prompt = prompt
      self.opt.outdir = outdir
      self.opt.seed = seed

      model = self.model

      if self.opt.plms:
          sampler = PLMSSampler(model)
      elif self.opt.dpm:
          sampler = DPMSolverSampler(model)
      else:
          sampler = DDIMSampler(model)

      os.makedirs(self.opt.outdir, exist_ok=True)
      outpath = self.opt.outdir

      batch_size = self.opt.n_samples
      # n_rows = self.opt.n_rows if self.opt.n_rows > 0 else batch_size

      if not self.opt.from_file:
          prompt = self.opt.prompt
          assert prompt is not None
          data = [batch_size * [prompt]]

      else:
          print(f"reading prompts from {self.opt.from_file}")
          with open(self.opt.from_file, "r") as f:
              data = f.read().splitlines()
              data = [p for p in data for i in range(self.opt.repeat)]
              data = list(self.chunk(data, batch_size))

      sample_path = os.path.join(outpath, "samples")
      os.makedirs(sample_path, exist_ok=True)
      sample_count = 0
      base_count = len(os.listdir(sample_path))
      # grid_count = len(os.listdir(outpath)) - 1

      start_code = None
      if self.opt.fixed_code:
          start_code = torch.randn(
            [self.opt.n_samples, self.opt.C, self.opt.H // self.opt.f, self.opt.W // self.opt.f],
            device=self.device
          )

      precision_scope = autocast if self.opt.precision == "autocast" else nullcontext
      with torch.no_grad(), \
          precision_scope("cuda"), \
          model.ema_scope():
              all_samples = list()
              for n in trange(self.opt.n_iter, desc="Sampling"):
                  for prompts in tqdm(data, desc="data"):
                      uc = None
                      if self.opt.scale != 1.0:
                          uc = model.get_learned_conditioning(batch_size * [""])
                      if isinstance(prompts, tuple):
                          prompts = list(prompts)
                      c = model.get_learned_conditioning(prompts)
                      shape = [self.opt.C, self.opt.H // self.opt.f, self.opt.W // self.opt.f]
                      samples, _ = sampler.sample(S=self.opt.steps,
                                                      conditioning=c,
                                                      batch_size=self.opt.n_samples,
                                                      shape=shape,
                                                      verbose=False,
                                                      unconditional_guidance_scale=self.opt.scale,
                                                      unconditional_conditioning=uc,
                                                      eta=self.opt.ddim_eta,
                                                      x_T=start_code)

                      x_samples = model.decode_first_stage(samples)
                      x_samples = torch.clamp((x_samples + 1.0) / 2.0, min=0.0, max=1.0)

                      if x_samples.length == 1:
                        return self.save_sample(sample_path, base_count, x_samples[0])

                      for x_sample in x_samples:
                          self.save_sample(sample_path, base_count, x_sample)
                          base_count += 1
                          sample_count += 1

                      all_samples.append(x_samples)

              # # additionally, save as grid
              # grid = torch.stack(all_samples, 0)
              # grid = rearrange(grid, 'n b c h w -> (n b) c h w')
              # grid = make_grid(grid, nrow=n_rows)

              # # to image
              # grid = 255. * rearrange(grid, 'c h w -> h w c').cpu().numpy()
              # grid = Image.fromarray(grid.astype(np.uint8))
              # # grid = put_watermark(grid, wm_encoder)
              # grid.save(os.path.join(outpath, f'grid-{grid_count:04}.png'))
              # grid_count += 1

      # print(f"Your samples are ready and waiting for you here: \n{outpath} \n"
      #       f" \nEnjoy.")
