import subprocess as sp
import io
import os
import random
from uuid import uuid4

from naogi import NaogiModel, FileRenderer
from sd import Stablediffusion

class PathFileRenderer(FileRenderer):
    @classmethod
    def render(cls, path):
        with open(path, "rb") as file:
            bytes_io = io.BytesIO(file.read())
        bytes_io.seek(0)

        filename = os.path.basename(path)

        return super().render(bytes_io, filename=filename, content_type='image/png', downloadable=True)


class Model(NaogiModel):
    def load_model(self):
        sp.check_call([
            'wget',
            '-O', 'model.ckpt',
            'https://naogi-public.s3.eu-west-1.amazonaws.com/sd-v2-768-v-ema.ckpt'
        ])
        self.model = Stablediffusion(ckpt="model.ckpt")

    def prepare(self, params):
        self.prompt = str(params['prompt'])
        self.uuid = str(uuid4())
        self.output = '/tmp/' + self.uuid
        self.seed = int(params['seed']) if ('seed' in params) else int(random.random() * 1000)

    def predict(self):
        image_path = self.model.call(
            prompt=self.prompt,
            outdir=self.output,
            seed=self.seed
        )

        return image_path

    def renderer(self):
        return PathFileRenderer
